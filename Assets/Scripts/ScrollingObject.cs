﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingObject : MonoBehaviour 
{
	private Rigidbody2D rb2d;
	private SpelBesturing spelBesturing;

	// Use this for initialization
	void Start () 
	{
		//Get and store a reference to the Rigidbody2D attached to this GameObject.
		rb2d = GetComponent<Rigidbody2D>();
		spelBesturing = (SpelBesturing)GameObject.FindWithTag("SpelBesturing").GetComponent<SpelBesturing>();

		//Start the object moving.
		rb2d.velocity = new Vector2 (spelBesturing.schuifsnelheid, 0);
	}

	void Update()
	{
		// If the game is over, stop scrolling.
		if(spelBesturing.gameOver == true)
		{
			rb2d.velocity = Vector2.zero;
		}
	}
}
