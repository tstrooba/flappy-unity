/* 
 * Dit script was gemaakt met Vubbi.
 * 
 * PAS OP: Aanpassingen aan deze code zullen verloren gaan als je opnieuw opslaat vanuit Vubbi.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpelBesturing : MonoBehaviour
{
	public GameObject scoreTekst;
	public GameObject gameOverTekst;
	public float score = 0f;
	public bool gameOver = false;
	public float schuifsnelheid = -1.5f;

	void Update()
	{
		if (gameOver && Input.GetKeyDown(KeyCode.Mouse0)) {
			UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
		}
	}

	void Msg_VogelIsGestorven()
	{
		(gameOverTekst.GetComponent<UnityEngine.UI.Text>()).text = "Game Over";
		gameOver = true;
	}

	void Msg_VogelMaaktPunt()
	{
		if (!gameOver) {
			score = score + 1f;
			(scoreTekst.GetComponent<UnityEngine.UI.Text>()).text = "Score: " + System.Convert.ToString(score);
		}
	}
}