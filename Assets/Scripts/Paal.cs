/* 
 * Dit script was gemaakt met Vubbi.
 * 
 * PAS OP: Aanpassingen aan deze code zullen verloren gaan als je opnieuw opslaat vanuit Vubbi.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Paal : MonoBehaviour
{
	public GameObject other;
	public GameObject SpelBesturing;

	void Start()
	{
		SpelBesturing = GameObject.FindWithTag("SpelBesturing");
	}

	void OnTriggerEnter2D(Collider2D otherCollider)
	{
		other = otherCollider.gameObject;
		if (other.CompareTag("Vogel")) {
			SpelBesturing.SendMessage("Msg_VogelMaaktPunt");
		}
	}
}