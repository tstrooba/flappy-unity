/* 
 * Dit script was gemaakt met Vubbi.
 * 
 * PAS OP: Aanpassingen aan deze code zullen verloren gaan als je opnieuw opslaat vanuit Vubbi.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Vogel : MonoBehaviour
{
	public bool isDood = false;
	public GameObject SpelBesturing;
	public GameObject AnimatieBesturing;
	public float opwaardseKracht = 200f;

	void Start()
	{
	}

	void Update()
	{
		if (!isDood) {
			if (Input.GetKeyDown(KeyCode.Mouse0)) {
				AnimatieBesturing.SendMessage("Msg_SpeelVliegAnimatie");
				(GetComponent<Rigidbody2D>()).velocity = new Vector3(0f, 0f, 0f);
				(GetComponent<Rigidbody2D>()).AddForce(new Vector3(0f, opwaardseKracht, 0f));
			}
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		(GetComponent<Rigidbody2D>()).velocity = new Vector3(0f, 0f, 0f);
		isDood = true;
		AnimatieBesturing.SendMessage("Msg_SpeelDoodAnimatie");
		SpelBesturing.SendMessage("Msg_VogelIsGestorven");
	}
}