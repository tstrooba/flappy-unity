﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VogelAnimatieBesturing : MonoBehaviour {

    private Animator anim; 

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
    public void Msg_SpeelDoodAnimatie() {
        anim.SetTrigger("Die");
    }

    public void Msg_SpeelVliegAnimatie()
    {
        anim.SetTrigger("Flap");
    }
}
